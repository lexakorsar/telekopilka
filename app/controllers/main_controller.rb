class MainController < ApplicationController
  def index
  end

  def save
    @email = RequestEmail.new()
    @email.email = params[:request_email][:email]
    if @email.save && params[:request_email][:email].present?
      SystemMailer.send_registration_notice(@email).deliver
      redirect_to root_url
    else
      redirect_to root_url
    end
  end
end
