class Client::BaseClientController < ApplicationController
  layout "client"

  helper_method :current_user

  before_filter :check_registration

  def check_registration
    unless current_user
      redirect_to client_log_in_url
    end

  end

  private
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
end
