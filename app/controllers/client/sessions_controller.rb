class Client::SessionsController < Client::BaseClientController

  skip_before_filter :check_registration

  def new
    #puts session.inspect
  end

  def create
    user = User.authenticate(params[:email], params[:password])
    puts user.inspect
    if user
      session[:user_id] = user.objectId
      redirect_to client_root_url
    else
      flash.now.alert = ""
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to client_root_url
  end

end
