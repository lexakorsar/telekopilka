class Partner::BasePartnerController < ApplicationController
  layout "partner"

  helper_method :current_partner

  before_filter :check_registration

  def check_registration
    unless current_partner
      redirect_to partner_log_in_url
    end

  end

  private
  def current_partner
    @current_partner ||= User.find(session[:partner_id]) if session[:partner_id]
  end
end
