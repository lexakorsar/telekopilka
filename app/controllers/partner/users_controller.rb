class Partner::UsersController < Partner::BasePartnerController

  skip_before_filter :check_registration

  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:users])
    @user.email = params[:users][:username]
    if @user.save
      PartnerMailer.send_registration_notice(@user).deliver
      redirect_to partner_root_url
    else
      render "new"
    end
  end
end
