class Partner::PaymentController < Partner::BasePartnerController
  before_filter :check_registration, :except => [:update_invoice, :pay_successful, :pay_declined]

  def index
    @income_payment = History.new
  end

  def create
    if params[:sum].to_i > 0
      type = HistoryType.find("HyAfqXR3Cq")
      course = CountCourse.find("NOXScP9bY7")
      count = params[:sum].to_i * course.coefficient.to_i
      @balance = History.create(
          :user_id => @current_partner.id,
          :invId => 0,
          :sum => params[:sum],
          :is_confirmed => false,
          :comment => t('views.payment.comment'),
          :is_ingoing => true,
          :history_type => type,
          :count => count
      )

      @current_partner.balance = @current_partner.balance.to_i + @balance.sum.to_i
      @current_partner.save

      inv_id = 0#@balance.id
      history_id = @balance.objectId
      inv_desc = "partner_#{@current_partner.id}"
      out_summ = @balance.sum
      culture = t('controllers.payment.culture')
      encoding = "utf-8";

      #mds = "#{mrh_login}:#{out_summ}:#{inv_id}:#{mrh_pass1}:ShpHistory_id=#{history_id}"
      mds = "#{YANDEX_CONFIG[:mrh_login]}:#{out_summ}:#{inv_id}:#{YANDEX_CONFIG[:mrh_pass1]}:Shp_HistoryId=#{history_id}"
      crc = Digest::MD5.hexdigest(mds);

      pay_params = "MrchLogin=#{YANDEX_CONFIG[:mrh_login]}&InvId=#{inv_id}&Culture=#{culture}&OutSum=#{out_summ}" +
                                  "&Desc=#{inv_desc}&SignatureValue=#{crc}" +
                                  "&Encoding=#{encoding}&Shp_HistoryId=#{history_id}"

      redirect_to YANDEX_CONFIG[:mrh_url] + "?" + URI.encode(pay_params)
    else
      @balance = History.new
      flash[:notice] = t('views.payment.error_sum')
      render :action => :new
    end
  end

  def pay_successful
    @balance = History.where(:objectId => params[:Shp_HistoryId], :is_confirmed => true).first
    if @balance.present?

      inv_id = 0
      out_summ = @balance.sum

      my_crc = Digest::MD5.hexdigest("#{out_summ}:#{inv_id}:#{YANDEX_CONFIG[:mrh_pass1]}:Shp_HistoryId=#{@balance.id}").upcase
      they_crc = params[:SignatureValue].upcase
      params_crc = Digest::MD5.hexdigest("#{params[:OutSum]}:#{params[:InvId]}:#{YANDEX_CONFIG[:mrh_pass1]}:Shp_HistoryId=#{params[:history_id]}").upcase

      if (my_crc == they_crc) && (they_crc == params_crc)
        flash[:notice] = t('views.payment.successful')
      end

      redirect_to partner_transaction_url
    else
      redirect_to partner_profile_url
    end

  end

  def pay_declined
    @balance = History.where(:objectId => params[:Shp_HistoryId], :is_confirmed => false).first
    if @balance.present?
      inv_id = 0
      out_summ = @balance.get_attribute('sum')
      my_crc = Digest::MD5.hexdigest("#{out_summ}:#{inv_id}:#{YANDEX_CONFIG[:mrh_pass1]}:Shp_HistoryId=#{@balance.id}").upcase
      they_crc = params[:SignatureValue].upcase
      params_crc = Digest::MD5.hexdigest("#{params[:OutSum]}:#{params[:InvId]}:#{YANDEX_CONFIG[:mrh_pass1]}:Shp_HistoryId=#{params[:history_id]}").upcase

      if (my_crc == they_crc) && (they_crc == params_crc)

        @balance.destroy
        flash[:notice] = t('views.payment.declined')
      end

      redirect_to partner_transaction_url
    else
      redirect_to root_url
    end
  end


  def update_invoice
    @balance = History.where(:objectId => params[:Shp_HistoryId], :is_confirmed => false, :is_ingoing => true).first
    #raise @balance.inspect
    if @balance.present?

      out_summ = @balance.get_attribute('sum')

      my_crc = Digest::MD5.hexdigest("#{out_summ}:#{params[:InvId]}:#{YANDEX_CONFIG[:mrh_pass2]}:Shp_HistoryId=#{@balance.objectId}").upcase
      they_crc = params[:SignatureValue].upcase
      params_crc = Digest::MD5.hexdigest("#{params[:OutSum]}:#{params[:InvId]}:#{YANDEX_CONFIG[:mrh_pass2]}:Shp_HistoryId=#{@balance.objectId}").upcase

      puts my_crc
      puts they_crc
      puts params_crc
      if (my_crc == they_crc) && (they_crc == params_crc)
        @balance.invId = params[:InvId].to_i
        @balance.is_confirmed = true
        @balance.is_ingoing = false
        if @balance.save

          render :text => "OK#{params[:InvId]}"
        else
          render :text => @balance.errors.inspect
        end
      else
        render :text => "Bad sign1"
      end
    else
      render :text => "Bad sign2"
    end

  end


end
