class Partner::HistoryController < Partner::BasePartnerController

  def index
    #@history = History.page(params[:page]).where(:is_ingoing => true)
    @history = Kaminari.paginate_array(History.all).page(params[:page]).per(10)
    #@history = History.page(params[:page])
  end

end