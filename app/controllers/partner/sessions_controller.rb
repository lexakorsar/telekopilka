class Partner::SessionsController < Partner::BasePartnerController
  skip_before_filter :check_registration

  def new
  end

  def create
    user = User.authenticate(params[:email], params[:password])
    if user
      session[:partner_id] = user.objectId
      redirect_to partner_root_url
    else
      flash.now.alert = t(:error_auth)
      render "new"
    end
  end

  def destroy
    session[:partner_id] = nil
    redirect_to partner_root_url
  end
end
