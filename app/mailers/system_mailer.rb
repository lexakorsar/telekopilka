class SystemMailer < ActionMailer::Base
  default from: "robot@telekopilka.com"

  def send_registration_notice(mail)
    @mail = mail
    mail(to: @mail.email, subject: 'noreplay')
  end
end
