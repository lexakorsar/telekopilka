class PartnerMailer < ActionMailer::Base
  default from: "robot@telekopilka.com"

  def send_registration_notice(user)
    @user = user
    mail(to: @user.email, subject: 'noreplay')
  end
end
