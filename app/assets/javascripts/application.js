// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//



//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require libs/fullPage
//= require libs/slimscroll

$(document).ready(function () {
    $.fn.fullpage({
        anchors: ['firstPage', 'secondPage', '3rdPage', '4thpage', 'lastPage'],
        menu: '#menu',
        scrollingSpeed: 200,
        afterSlideLoad: function( anchorLink, index, slideAnchor, slideIndex){
            if (anchorLink == "firstPage" && slideIndex == 1) {


                $("#slide-1-2").find('#iphone-container .phone-2').delay(50).animate({
                    left: '176px'
                }, 800, 'easeOutExpo');
                $("#slide-1-2").find('#iphone-container .phone-3').delay(50).animate({
                    left: '336px'
                }, 800, 'easeOutExpo');
            }
        },
        afterLoad: function (anchorLink, index) {

            if (index == 1) {
                //moving the image
                $('#section1').find('#separate-iphone img').delay(300).animate({
                    left: '0%'
                }, 1500, 'easeOutExpo');
            }

        },

        onLeave: function(index, direction){
            if (index == 1 && direction =='down' ) {
//                alert("Going to section 3!");
                $('#section1').find('#separate-iphone img').animate({
                    left: '150%'
                }, 1500, 'easeOutExpo');
            }
        }

    });

});

