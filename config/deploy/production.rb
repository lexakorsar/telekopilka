set :stage, :production

set :deploy_to, '/home/deploy/telekopilka'

set :branch, 'master'

set :rails_env, 'production'

# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
server '13.58.7.77', user: 'deploy', roles: %w{app db web}