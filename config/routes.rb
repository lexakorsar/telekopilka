TelekopilkaPromo::Application.routes.draw do
  root "main#index"
  post "main/save"


  namespace :client do
    get "log_in" => "sessions#new", :as => "log_in"
    get "log_out" => "sessions#destroy", :as => "log_out"

    get "sign_up" => "users#new", :as => "sign_up"

    resource :profile
    resources :transactions
    resources :users
    resources :sessions
    root "home#index"
  end

  namespace :partner do
    get "log_in" => "sessions#new", :as => "log_in"
    get "log_out" => "sessions#destroy", :as => "log_out"

    get "sign_up" => "users#new", :as => "sign_up"

    get "history/index"

    get "payment/index" , :as => "payment"
    post "payment/create"

    resource :profile
    resources :transactions
    resources :users
    resources :sessions
    root "home#index"
  end

  namespace :investor do
  end

  get "/update_invoice", :controller => "partner/payment", :action => "update_invoice"
  get "/pay_declined", :controller => "partner/payment", :action => "pay_declined"
  get "/pay_successful", :controller => "partner/payment", :action => "pay_successful"

end
