class CreateRequestEmails < ActiveRecord::Migration
  def change
    create_table :request_emails do |t|
      t.text :email

      t.timestamps
    end
  end
end
